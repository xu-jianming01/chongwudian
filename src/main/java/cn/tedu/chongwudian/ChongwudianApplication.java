package cn.tedu.chongwudian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChongwudianApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChongwudianApplication.class, args);
    }

}
